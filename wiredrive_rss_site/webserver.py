from flask import Flask, render_template
app = Flask(__name__)

from rssfeed import get_rss_items

@app.route("/")
def rss_feed_view():
    error = None
    sample_rss_feed_url = "http://www.wdcdn.net/rss/presentation/library/client/iowa/id/128b053b916ea1f7f20233e8a26bc45d"
    items = get_rss_items(sample_rss_feed_url)
    return render_template("feed_list.html", items=items)


if __name__ == "__main__":
    # Runs on port 5000 by default
    app.run()