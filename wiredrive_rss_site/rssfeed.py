import os
import sys
import pytz
import time
import datetime
import calendar

import feedparser
import subprocess


class FeedItem(object):

    """Contains and sanitize information from the RSS Feed.

    When sanitizing, it will drop any values equal to 0 or None.
    This class will still retain those values in an untouched attribute,
    "entry". Otherwise, sanitized information may be accessed via the
    "sanitized_entry" attribute.
    """

    def __init__(self, rss_entry=None):
        """Pass in the parsed RSS item from the feed to intialize."""
        if not rss_entry:
            rss_entry = {}

        self.entry = rss_entry

    @property
    def content_url(self):
        """Return item's media content url or an empty string if unavailable."""
        try:
            url = self.entry.media_content[0]['url']
        except AttributeError:
            url = ""
        finally:
            return url

    @property
    def title(self):
        """Return the item's title as string or 'N/A' if not available."""
        try:
            title = self.entry.title
        except AttributeError:
            title = "N/A"
        finally:
            return title

    def pull_content_metadata(self):
        """Use ffprobe to find metadata from content at url.

        Return a dictionary with duration, bitrate,  returned by ffprobe.

        ffprobe outputs information through stderr, so redirect it to stdout.
        Use Popen instead of check_output because redirecting stderr can
        cause a deadlock as per the documentation:
        https://docs.python.org/2/library/subprocess.html#subprocess.check_output

        ffprobe options used:
        -i <file>: specifies the input file (or url, in this case) to check
        -hide_banner: will suppress ffprobe information about build
        """
        ffprobe_command = ["ffprobe", "-i", self.content_url, "-hide_banner"]
        try:
            p = subprocess.Popen(ffprobe_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except WindowsError:
            # Executable has been moved most likely. Use default metadata.
            self.add_default_metadata()
        else:
            ffprobe_output = p.communicate()[0]
            self.parse_metadata(ffprobe_output)

    def add_default_metadata(self):
        """Use N/A for fields that cannot be retrieved by ffprobe."""
        keys = ["bitrate", "duration", "video_compression"]
        default_metadata = {key: "N/A" for key in keys}

        # Combine default metadata with entry dictionary
        self.entry.update(default_metadata)

    def parse_metadata(self, ffprobe_output):
        """Retrieve bitrate, duration, and compression from ffprobe output."""
        for line in ffprobe_output.splitlines():
            # Preprocess the line to lowercase and remove whitespace
            line = line.strip().lower()
            if "no such file" in line:
                self.add_default_metadata()
                return
            elif line.startswith("duration:"):
                self.parse_duration_and_bitrate(line)
            elif line.startswith("stream") and "video:" in line:
                self.parse_video_compression(line)

        if "bitrate" not in self.entry:
            # Catch-all in case of other invalid output from ffprobe.
            self.add_default_metadata()

    def parse_video_compression(self, ffprobe_line):
        """Split video element from line and update entry's video compression.

        Sample Good Line:
        stream #0:1(eng): video: h264 (main) (avc1 / 0x31637661), \
            yuv420p(tv, smpte170m), 480x360, 1253 kb/s, 29.97 fps, \
            29.97 tbr, 2997 tbn, 5994 tbc (default)
        """

        # If there's already video compression set for item, ignore.
        if "video_compression" in self.entry:
            return

        try:
            full_compression = ffprobe_line.split("video: ")[1]
        except IndexError:
            compression = "N/A"
        else:
            # Information beyond 2nd comma separated element is repeated
            # elsewhere in the model.
            shortened_compression_list = full_compression.split(",")[:3]
            compression = ",".join(shortened_compression_list)

        self.entry["video_compression"] = compression

    def parse_duration_and_bitrate(self, duration_line):
        """Pull information from the ffprobe output on the file.

        Sample Line:
            Duration: 00:00:30.36, start: 0.000000, bitrate: 1401 kb/s
        """
        if not duration_line.startswith("duration"):
            # Then it is an invalid line for these purposes.
            return

        metadata = {}
        raw_elements = duration_line.split(", ")
        for element in raw_elements:
            key, value = element.split(": ")
            if key == "start":
                continue

            self.entry[key] = value

    def sanitize(self):
        """Remove unwanted values from 'entry' and store as 'sanitized_entry'."""
        self.sanitized_entry = sanitize_item(self.entry)

    @property
    def smallest_thumbnail(self):
        if not hasattr(self, "_smallest_thumbnail"):
            self.determine_smallest_thumbnail()

        return self._smallest_thumbnail

    def determine_smallest_thumbnail(self):
        """Use height to determine the smallest available thumbnail."""
        smallest_thumbnail = None
        for thumbnail in self.entry["media_thumbnail"]:
            height = thumbnail['height']
            width = thumbnail['width']
            if not smallest_thumbnail or height < smallest_thumbnail['height']:
                smallest_thumbnail = thumbnail

        self._smallest_thumbnail = smallest_thumbnail


def parse_feed(feed_url):
    """Return parsed feed dictionary.

    Bad feeds will show up as "bozo" feeds, meaning their bozo bit equals 1.
    Properly parsed feeds will show up with a bozo bit of 0.
    More information: https://pythonhosted.org/feedparser/bozo.html

    Examples of "bad feed" conditions include:
        1. Malformed XML
        2. Unavailable internet connection
        3: Url without content
    """
    return feedparser.parse(feed_url)


def build_item_list(feed):
    """Return a list of dictionaries with updated media metadata.

    Arguments:
        feed: a list of parsed dictionaries from RSS feed
    """
    item_list = []
    for entry in feed.entries:
        item = FeedItem(entry)
        item.pull_content_metadata()
        item.sanitize()
        item_list.append(item)

    return item_list


def get_rss_items(rss_feed_url):
    """Return a parsed list of items from the RSS feed.

    If there is an issue when retrieving the feed, do not return a list.
    """
    parsed_feed = parse_feed(rss_feed_url)
    if parsed_feed.bozo == 1:
        print("Encountered issue parsing provided URL: %s." % parsed_feed.bozo_exception)
        print("Feed may be invalid.")

    item_list = build_item_list(parsed_feed)
    return item_list


def sanitize_item(item):
    """Remove null elements from item and correct errors. Return dictionary."""
    new_item = {}
    for key, value in item.iteritems():
        if key == "summary" or key == "summary_detail":
            # Not originally in the RSS feed so it should be removed.
            continue

        if type(value) is feedparser.FeedParserDict:
            value = sanitize_item(value)
        elif type(value) is time.struct_time:
            value = format_time(value)

        if "published" in key:
            # Remove "_parsed" from "published_parsed" key
            key = "published"

        if value == '0' or (not value and value is not False):
            # We do not want to display null or 0 values in page
            # False, however, is still a valid value.
            continue

        new_item[key] = value

    return new_item


def format_time(time_struct):
    """Format time object into Pacific Time. Return time string.

    Feedparser, by default, parses datetimes from feeds into Python
    time structures. The goal was to print all values of the
    dictionary and a parsed datetime is not natively in the feed. As such,
    this step is to maintain parity with the feed's unedited data.
    A nice side-effect of this is that it would be easier to repurpose,
    although one could argue "YAGNI".
    """
    pacific = pytz.timezone("America/Los_Angeles")

    # This format matches that of the provided XML stream
    time_format = "%a, %d %b %Y %H:%M:%S %z"

    # From https://www.virag.si/2012/10/converting-utc-struct_time-to-datetime-in-python/
    utc_datetime = datetime.datetime.fromtimestamp(calendar.timegm(time_struct), tz=pytz.utc)

    # Take the UTC datetime and convert it to PST.
    localized_time = utc_datetime.replace(tzinfo=pytz.utc).astimezone(pacific)
    return localized_time.strftime(time_format)



