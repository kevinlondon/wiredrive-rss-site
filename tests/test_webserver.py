import urllib2
import unittest
from selenium import webdriver

from flask.ext.testing import LiveServerTestCase


class ServerTest(unittest.TestCase):

	"""Check that page loads and does not contain null values."""

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.addCleanup(self.browser.quit)

	def create_app(self):
		app = Flask(__name__)
		app.config['TESTING'] = True
		return app

	@unittest.skip("Currently broken in this version of Flask-Testing.")
	def test_server_is_up_and_running(self):
		response = urllib2.urlopen(self.get_server_url())
		self.assertEquals(response.code, 200)

	def test_hosted_static_page(self):
		"""Check that None does not appear within the hosted page."""
		hosted_url = "https://kevinlondon.bitbucket.org/"
		self.browser.get(hosted_url)
		self.assertNotIn("None",  self.browser.page_source)

