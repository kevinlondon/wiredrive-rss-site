import unittest
import os
import shutil

from wiredrive_rss_site import rssfeed


feed_url = "http://www.wdcdn.net/rss/presentation/library/client/iowa/id/128b053b916ea1f7f20233e8a26bc45d"


class TestParseURL(unittest.TestCase):
    def test_valid_url(self):
        feed = rssfeed.parse_feed(feed_url)
        self.assertIsNotNone(feed)
        self.assertIsNotNone(len(feed.entries))

        # A correct feed should have a bozo value of 0.
        self.assertEquals(feed.bozo, 0)

        # This test would fail without a valid internet connection.
        first_entry = feed.entries[0]
        self.assertIn("id", first_entry)

    def test_invalid_url(self):
        invalid_url = "http://localhost"
        feed = rssfeed.parse_feed(invalid_url)
        self.assertEquals(len(feed.entries), 0)

        # Bozo bit indicates that it is a bad feed.
        self.assertEquals(feed.bozo, 1)

    def test_missing_url(self):
        feed = rssfeed.parse_feed(None)
        self.assertEquals(feed.bozo, 1)


class TestProperties(unittest.TestCase):
    def test_missing_item_properties(self):
        bad_item = rssfeed.FeedItem(None)
        self.assertEquals(bad_item.content_url, "")
        self.assertEquals(bad_item.title, "N/A")

    def test_missing_url_from_content(self):
        # Initialize with an empty dictionary
        bad_item = rssfeed.FeedItem({})

        # Provide additional empty dictionaries where url would be.
        bad_item.entry['media_content'] = [{}, {}]
        self.assertEquals(bad_item.content_url, "")

    def test_good_properties(self):
        # All the values should be correct.
        pulled_feed = rssfeed.parse_feed(feed_url)
        entry = pulled_feed.entries[0]
        item = rssfeed.FeedItem(entry)

        self.assertEquals(item.title, "Phone")
        url = (u"http://cache.wdcdn.net/asset/assetId"
               "/21239/size/primary/ts/1369185308/type/library/client/WD-BEPV0"
               "/BB_MTVCoL_PhoneDC.mov?token=128b053b916ea1f7f20233e8a26bc45d"
               "&category=pres&action=view")
        self.assertEquals(item.content_url, url)


class TestPullingMetadata(unittest.TestCase):
    def test_missing_ffprobe(self):
        """Move ffprobe and check that it still iniitalizes metadata.

        Note: This will fail on OSX / Linux.
        """
        shutil.move("ffprobe.exe", "../")
        item = rssfeed.FeedItem()
        item.pull_content_metadata()
        self.assertEquals(item.entry['bitrate'], "N/A")
        shutil.move("../ffprobe.exe", ".")

    @unittest.skip("Unavailable on Windows.")
    def test_no_exec_permission_for_ffprobe(self):
        """Disable executable permission for ffprobe and check output.

        Wiredrive servers have no exeuctable permission to underlying
        filesystem. This is designed to replicate that environment.
        """
        # Cannot do on Windows as Windows does not support full chmod.
        # Documentation: https://docs.python.org/2/library/os.html#os.chmod

    def test_invalid_media_content_url(self):
        item = rssfeed.FeedItem({})
        self.assertEquals(item.content_url, "")
        item.pull_content_metadata()
        self.assertEquals(item.entry['bitrate'], "N/A")
        self.assertEquals(item.entry['duration'], "N/A")
        self.assertEquals(item.entry['video_compression'], "N/A")

    def test_good_metadata(self):
        # Get a valid FeedItem from the RSS Feed
        pulled_feed = rssfeed.parse_feed(feed_url)
        entry = pulled_feed.entries[0]
        item = rssfeed.FeedItem(entry)
        item.pull_content_metadata()

        # Should not see any default metadata values.
        self.assertEquals(item.entry['bitrate'], "1401 kb/s")
        self.assertEquals(item.entry['duration'], "00:00:30.36")
        self.assertNotEquals(item.entry['video_compression'], "N/A")

    def test_parse_empty_line(self):
        """Empty or incorrect lines should still set default metadata values."""
        item = rssfeed.FeedItem()
        item.parse_metadata(ffprobe_output="")
        self.assertEquals(item.entry["bitrate"], "N/A")


class TestVideoCompressionParsing(unittest.TestCase):
    def test_parsing_video_compression_good(self):
        """Provide a valid video compression line and see if it shortens."""
        item = rssfeed.FeedItem()
        good_line = [
            "stream #0:1(eng): video: h264 (main) (avc1 / 0x31637661),",
            "yuv420p(tv, smpte170m), 480x360, 1253 kb/s, 29.97 fps,",
            "29.97 tbr, 2997 tbn, 5994 tbc (default)"
        ]

        compression = "h264 (main) (avc1 / 0x31637661),yuv420p(tv, smpte170m)"
        item.parse_video_compression("".join(good_line))
        self.assertEquals(item.entry['video_compression'], compression)

    def test_parsing_video_compression_bad(self):
        """Provide incomplete lines for video compression."""
        item = rssfeed.FeedItem()
        compression = "nothing good really"
        bad_line = "stream #0:1(eng): video: %s" % compression
        item.parse_video_compression(bad_line)
        self.assertEquals(item.entry['video_compression'], compression)

    def test_parsing_multiple_lines_of_video_compression(self):
        """FeedItem should only use the first video compression line found."""
        item = rssfeed.FeedItem()
        good_line_1 = "stream #0:1(eng): video: h264"
        good_line_2 = "stream #1:1(ger): video: prores444"
        item.parse_video_compression(good_line_1)
        item.parse_video_compression(good_line_2)

        # Check that we're using first line compresssion.
        self.assertEquals(item.entry['video_compression'], "h264")

    def test_parsing_bad_line(self):
        """Should return "N/A" for incomplete video compression line."""
        item = rssfeed.FeedItem()
        bad_line = "stream #0:1(eng): audio: it's audio"
        item.parse_video_compression(bad_line)
        self.assertEquals(item.entry['video_compression'], "N/A")


class TestParsingDuration(unittest.TestCase):
    def test_valid_line(self):
        valid_line = "Duration: 00:00:30.36, start: 0.000000, bitrate: 1401 kb/s"
        item = rssfeed.FeedItem()

        # All input lines will be converted to lowercase
        item.parse_duration_and_bitrate(valid_line.lower())
        self.assertEquals(item.entry['duration'], "00:00:30.36")
        self.assertEquals(item.entry['bitrate'], "1401 kb/s")
        self.assertNotIn("start", item.entry)

    def test_invalid_line(self):
        invalid_line = "Something Else: ABCDEF, duration: a time"
        item = rssfeed.FeedItem()
        item.parse_duration_and_bitrate(invalid_line)

        # If it dossn't start with duration, it's an invalid line.
        # So there should be no key for duration in the entry.
        self.assertNotIn("duration", item.entry)


class TestSmallestThumbnail(unittest.TestCase):
    def test_finding_smallest_thumbnail(self):
        item = rssfeed.FeedItem()
        thumbnails = [
            {'height': 200, 'width': 100}, {"height": 100, "width": 50}
        ]
        item.entry['media_thumbnail'] = thumbnails
        self.assertEquals(item.smallest_thumbnail['height'], 100)


class TestGettingRSSItems(unittest.TestCase):
    @unittest.skip("Takes a long time.")
    def test_build_item_list(self):
        # Use global feed_url
        rss_items = rssfeed.get_rss_items(feed_url)
        self.assertIsNotNone(len(rss_items))
        self.assertEquals(type(rss_items), list)

    @unittest.skip("Takes a long time.")
    def test_bad_rss_items(self):
        rss_items = rssfeed.get_rss_items("bad_url")
        # It shouldn't error but it should give us an empty liwt.
        self.assertEquals(rss_items, [])


class TestRemovingNullValues(unittest.TestCase):
    def setUp(self):
        entry = rssfeed.parse_feed(feed_url).entries[0]
        self.sanitized_item = rssfeed.sanitize_item(entry)

    def test_sanitize_item(self):
        url = u"sample_url"
        original_item = {'base': url, 'value': u'0', 'language': None, 'falsey': False}
        sanitized_item = rssfeed.sanitize_item(original_item)

        # Should remove items with null or 0 but keep False
        self.assertNotIn("language", sanitized_item)
        self.assertNotIn("value", sanitized_item)
        self.assertIn("falsey", sanitized_item)

        # This is a normal value, should be in there.
        self.assertIn("base", sanitized_item)
        self.assertEquals(sanitized_item['base'], url)
        self.assertEquals(sanitized_item['base'],url)

    def test_converting_date_in_dict(self):
        """Should have modified published_parsed to published."""
        self.assertIn("published", self.sanitized_item)
        self.assertNotIn("published_parsed", self.sanitized_item)
        self.assertEquals(self.sanitized_item['published'], "Fri, 18 Mar 2011 00:36:19 -0700")

    def test_removing_additional_feedparser_tags(self):
        # Not in the original RSS feeds so it too should be absent.
        self.assertNotIn("summary_detail", self.sanitized_item)
        self.assertNotIn("summary", self.sanitized_item)

