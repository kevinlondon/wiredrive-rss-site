# Wiredrive RSS Site

###To Install:

Please install any missing requirements by typing `pip install -r requirements.txt`
from within the base of the repository.

This application also relies upon `ffprobe` from the ffmpeg suite, so please install that as well. [ffprobe](http://www.ffmpeg.org/) 
pulls in additional metadata, such as duration and bitrate, for items in the RSS feed.


###Running the Server:

If you're running on Windows, place the `ffprobe.exe` within the root of the repository. You can run the server with this command:

    python ./wiredrive_rss_site/webserver.py

The server will run at `http://localhost:5000` by default and it may take a few moments to load.

###Running the Tests

From the root of the project, type:

    nosetests tests